﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Indiv
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Texture2D spr;
        public int squareCount = 20;
        laser l1;
        laser  l2;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 1920;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            base.Initialize();
        }

  
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), spriteBatch);
            spr = Content.Load<Texture2D>("sprites");
            l1 = new laser(this, ref spr, new Rectangle(0, 0, 400, 50), new Vector2(2320, 360));
            Components.Add(l1);
            l2 = new laser(this, ref spr, new Rectangle(0, 0, 400, 50), new Vector2(2320, 720));
            Components.Add(l2);

            for (int i = 0; i < 10; i++)
            {
                CreateEnemy(i);       
            }
        }

        protected void CreateEnemy(int i)
        {
            enemy en = new enemy(this, this, ref spr, new Rectangle(0,100, 50, 50), new Vector2(640, 1030), ref squareCount, i * 4, ref l1, ref l2);
            Components.Add(en);
            enemy en2 = new enemy(this, this, ref spr, new Rectangle(0, 100, 50, 50), new Vector2(1280, 1030), ref squareCount, i * 4, ref l1, ref l2);
            Components.Add(en2);
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || (squareCount == 0) || gameTime.TotalGameTime.Minutes==2)
            {
                Exit();
            }

            base.Update(gameTime);
        }



        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkSlateGray);

   

            spriteBatch.Begin();
           // spriteBatch.Draw(sprscales, Vector2.Zero, Color.White);

            base.Draw(gameTime);
            spriteBatch.End();
        }
    }
}
