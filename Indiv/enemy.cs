﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Indiv
{

    public partial class enemy : DrawableGameComponent
    {
        private Texture2D tex;
        private Rectangle rec;
        private Vector2 pos;
        private int speed;
        private Random rand;
        private Random rand2;
       // private Random l;
       // private Random r;
        private Rectangle screen;
        private bool up;
       
        Game1 game1;
        Game game;
        laser l1, l2;

        public enemy(Game g, Game1 g1,  ref Texture2D texture, Rectangle rectangle, Vector2 position, ref int squareCount, int seed, ref laser las1, ref laser las2):base(g)
        {
            tex = texture;
            rec = rectangle;
            pos = position;
            up = true;
            game = g;
            game1 = g1;
            l1 = las1;
            l2 = las2;
            
            rand = new Random(seed);
            speed = rand.Next(1, 10);
            //SetSpeed();
    
            screen = new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height);
            //count = squareCount;
            InitializeComponent();
        }

        //private void SetSpeed()
        //{
        //    
        //    speed = rand.Next(1, 15);
            
        //}
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sprBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            sprBatch.Draw(tex, pos, rec, Color.White);
            base.Draw(gameTime);
        }
        public override void Update(GameTime gameTime)
        {

            if (IsCollide(l1))
            {
                game1.squareCount--;
                l1.Collision();
                game.Components.Remove(this);
            }

            if (IsCollide(l2))
            {
                game1.squareCount--;
                l2.Collision();
                game.Components.Remove(this);
            }

            if (up)
            {
                pos.Y -= speed;
            }
            else
            {
                pos.Y += speed;
            }

            if (pos.Y < screen.Top)
            {
                up = false;
            }
            if (pos.Y > screen.Height - rec.Height)
            {
                up = true;
            }  
        }


        bool IsCollide(laser l)
        {
            Vector2 laserpos = l.GetPosition();
            Rectangle laserrec = l.GetRectangle();
            if (laserpos.X < pos.X + rec.Width &&
            laserpos.X + laserrec.Width > pos.X &&
            laserpos.Y < pos.Y + rec.Height &&
            laserpos.Y + laserrec.Height > pos.Y)
            {
                return true;
            }
            else return false;
        }
    }
}
