﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Indiv
{
    public partial class laser : DrawableGameComponent
    {
        private Texture2D tex;
        private Rectangle rec;
        private Vector2 pos;
        private bool shot = false;
        private Vector2 initial;
        private int speed = 30;

        public laser(Game game, ref Texture2D texture, Rectangle rectangle, Vector2 position) :base(game)
        {
            tex = texture;
            rec = rectangle;
            pos = position;
            initial = position;
           
            InitializeComponent();
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sprBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            sprBatch.Draw(tex, pos, rec, Color.White);
             base.Draw(gameTime);
        }
        public override void Update(GameTime gameTime)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.Space) && shot == false)
            {
                shot = true;
            }
            if (shot)
            {
                pos.X -= speed;
            }
            if (pos.X <= -rec.Width)
            {
                pos = initial;
                shot = false;
            }

        }

        public Vector2 GetPosition()
        {
            return pos;
        }
        public Rectangle GetRectangle()
        {
            return rec;
        }

        public void Collision()
        {
            pos = initial;
            shot = false;
        }

    }
}
